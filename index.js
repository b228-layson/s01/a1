

/* 
1. Create a function named addToEnd that will add a passed in string to the end of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Ryan" as arguments when testing.
*/

let students = [
    "John",
    "Joe",
    "Jane",
    "Jessie"
];

function addToEnd(x, y){
    if(typeof y === 'string'){
        x.push(y);
        console.log(x);
    }
    else{
        console.log("error - can only add strings to an array");
        
    }

}

/* 
addToEnd(students, "Ryan");
addToEnd(students, 045);
*/


/* 

2. Create a function named addToStart that will add a passed in string to the start of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Tess" as arguments when testing. 
*/


function addToStart(x, y) {
    if (typeof y === 'string') {
        x.unshift(y);
        console.log(x);
    }
    else {
        console.log("error - can only add strings to an array");

    }

}

/* 
addToStart(students, "Tess");
addToStart(students, 034);
*/

/* 


3. Create a function named elementChecker that will check a passed in array if at least one of its elements has the same value as a passed in argument. If array is empty, return the message "error - passed in array is empty". Otherwise, return a boolean value depending on the result of the check. Use the students array and the string "Jane" as arguments when testing.

*/

function elementChecker(x, y){
    if (x.length && y.length >= 0){
        for(i=0; i<x.length; i++){
            if (x[i] === y){
                return true;
            }
        }
    }
    else{
        console.log("error - passed in array is empty");
    }
}

/* 
elementChecker(students, "Jane");
elementChecker([], "Jane");
*/

/* 

4.    Create a function named checkAllStringsEnding that will accept a passed in array and a character. The function will do the ff:
if array is empty, return "error - array must NOT be empty"
if at least one array element is NOT a string, return "error - all array elements must be strings"
if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
if every element in the array ends in the passed in character, return true. Otherwise return false.
Use the students array and the character "e" as arguments when testing.
*/



function checkAllStringsEnding(x, y) {

    if (x.length !== 0){
        let xtest = x.every((e)=> typeof e === 'string')
        if (xtest){
            if(typeof y === 'string'){
                if(y.length === 1){
                    console.log(students.every((e) => e[e.length - 1] === x));
                }
                else{
                    return "error - 2nd argument must be a single character";
                }
            }
            else{
                return "error - 2nd argument must be of data type string";
            }
        }
        else{
            return "error - all array elements must be strings";
        }
    }
    else{
        return "error - array must NOT be empty";
    }
    
}

/* 
checkAllStringsEnding(students, "e");
checkAllStringsEnding([], "e");
checkAllStringsEnding(["Jane", 02], "e");
checkAllStringsEnding(students, "el");
checkAllStringsEnding(students, 4);
*/


/* 


5.    Create a function named stringLengthSorter that will take in an array of strings as its argument and sort its elements in an ascending order based on their lengths. If at least one element is not a string, return "error - all array elements must be strings". Otherwise, return the sorted array. Use the students array to test.


*/

function stringLengthSorter(x) {
    let xtest = x.every((e) => typeof e === 'string')
    if(xtest){
        return x;
    }
    else{
        return "error - all array elements must be strings"
    }
    
}

/* 
    stringLengthSorter(students);
    stringLengthSorter([037, "John", 039, "Jane"]);
*/


/* 
6.    Create a function named startsWithCounter that will take in an array of strings and a single character. The function will do the ff:
if array is empty, return "error - array must NOT be empty"
if at least one array element is NOT a string, return "error - all array elements must be strings"
if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"

return the number of elements in the array that start with the character argument, must be case-insensitive
Use the students array and the character "J" as arguments when testing.

*/

function startsWithCounter(x, y) {

let ctr = 0;
    
    if (x.length !== 0) {
        let xtest = x.every((e) => typeof e === 'string')
        if (xtest) {
            if (typeof y === 'string') {
                if (y.length === 1) {
                    x.forEach(e => {
                        if (e[0].toLowerCase() === y || e[0].toUpperCase() === y)
                        ctr++;
                    });
                    /* console.log(x.every((e) => e.charAt(0) === y)); */
                    return ctr;
                }
                else {
                    return "error - 2nd argument must be a single character";
                }
            }
            else {
                return "error - 2nd argument must be of data type string";
            }
        }
        else {
            return "error - all array elements must be strings";
        }
    }
    else {
        return "error - array must NOT be empty";
    }

    
}



/* 
startsWithCounter(students, "j");
*/


/* 


7.    Create a function named likeFinder that will take in an array of strings and a string to be searched for. The function will do the ff:
if array is empty, return "error - array must NOT be empty"
if at least one array element is NOT a string, return "error - all array elements must be strings"
if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
return a new array containing all elements of the array argument that have the string argument in it, must be case-insensitive
Use the students array and the character "jo" as arguments when testing.
*/


function likeFinder(x, y) {

    let i = [];

    if (x.length !== 0) {
        let xtest = x.every((e) => typeof e === 'string')
        if (xtest) {
            if (typeof y === 'string') {
                
                   /*  console.log(students.every((e) => e[e.length - 1] === x)); */
                   x.forEach((e) => {
                       if (e.toUpperCase().includes(y) || e.toLowerCase().includes(y)){
                        i.push(e);
                
                   }}
                   );
                   
                   return i;
            }
                
            
            else {
                return "error - 2nd argument must be of data type string";
            }
        }
        else {
            return "error - all array elements must be strings";
        }
    }
    else {
        return "error - array must NOT be empty";
    }

}

/* 
likeFinder(students, "jo");
*/


/* 


8.    Create a function named randomPicker that will take in an array and output any one of its elements at random when invoked. Pass in the students array as an argument when testing.

*/

function randomPicker(x) {
    let y = x[Math.floor(Math.random() * x.length)];
    return y;
}

/* 
randomPicker(students);
*/